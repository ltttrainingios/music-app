//
//  MusicHelper.swift
//  training_music_app
//
//  Created by Nguyễn  Mạnh Đức on 10/11/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation
import AVFoundation
import RxSwift
import RxCocoa

protocol MusicDelegate: class {
    func playerItemDidStartPlay()
}

class MusicHelper {
    static let instance = MusicHelper()
    var playingList = [AVPlayerItem]()
    var player: AVPlayer? = AVPlayer()
    var musicList = [Item]()
    
    var playingIndex = BehaviorRelay<Int>(value: -1)
    var repeatType = BehaviorRelay<RepeatType>(value: RepeatType.NO_REPEAT)
    var isShuffle = BehaviorRelay<Bool>(value: false)
    
    let disposeBag = DisposeBag()
    weak var delegate: MusicDelegate?
    
    private init() {
        NotificationCenter.default.rx.notification(Notification.Name.AVPlayerItemDidPlayToEndTime)
            .asObservable().subscribe(onNext: { [weak self] notification in
                self?.next()
            }).disposed(by: disposeBag)
    }
    
    func addMusicListToQueue(list: [Item]) {
        musicList = list
        clearSession()
        for data in musicList {
            addToQueue(url: data.track?.previewURL ?? "")
        }
    }
    
    func clearSession() {
        player?.replaceCurrentItem(with: nil)
        playingIndex.accept(0)
        playingList = [AVPlayerItem]()
    }
    
    func addToQueue(url: String) {
        let musicUrl = URL(string: url)
        let playerItem = AVPlayerItem.init(url: musicUrl!)
        playingList.append(playerItem)
    }
    
    func setMusic(index: Int) {
        if index > getListCount() - 1 {
            return
        }
        if player?.rate != 0.0 {
            player?.pause()
        }
        playingList[index].seek(to: CMTimeMake(0, 1)) { (value) in
            if value {
                self.player?.replaceCurrentItem(with: self.playingList[index])
            }
        }
    }
    
    func getListCount() -> Int {
        return playingList.count
    }
    
    func play() {
        setMusic(index: playingIndex.value)
        player?.play()
    }
    
    func pause() {
        player?.pause()
    }
    
    func next() -> Bool {
        switch repeatType.value {
        case .NO_REPEAT:
            if playingIndex.value >= getListCount() - 1 {
                return false
            }
            playingIndex.accept(playingIndex.value + 1)
            play()
        case .REPEAT_ALL:
            if isShuffle.value {
                shufflePlayForRepeatAll()
            } else {
                if playingIndex.value >= getListCount() - 1 {
                    playingIndex.accept(0)
                } else {
                    playingIndex.accept(playingIndex.value + 1)
                }
            }
            play()
        case .REPEAT_ONE:
            play()
        }
        return true
    }
    
    func previous() {
        if playingIndex.value <= 0 {
            return
        }
        playingIndex.accept(playingIndex.value - 1)
        play()
    }
    
    func shufflePlayForRepeatAll() {
        var isValid = false
        var randomInt = 0
        while !isValid {
            randomInt = Int(arc4random_uniform(UInt32(getListCount())))
            if randomInt != playingIndex.value {
                isValid = true
            }
        }
        playingIndex.accept(randomInt)
    }
    
    func changeRepeatType() {
        switch repeatType.value {
        case .NO_REPEAT:
            repeatType.accept(.REPEAT_ALL)
        case .REPEAT_ONE:
            repeatType.accept(.NO_REPEAT)
        case .REPEAT_ALL:
            repeatType.accept(.REPEAT_ONE)
        }
    }
    
    func changeShuffle() {
        isShuffle.value ? isShuffle.accept(false) : isShuffle.accept(true)
    }
    
    func isPlaying() -> Bool {
        return player?.rate != 0.0
    }
}
