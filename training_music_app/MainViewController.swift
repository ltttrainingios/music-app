//
//  MainViewController.swift
//  training_music_app
//
//  Created by Nguyễn  Mạnh Đức on 10/10/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class MainViewController: UIViewController {
    @IBOutlet weak var miniBarView: UIView!
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var previousBtn: UIButton!
    
    let viewModel = MainViewModel()
    let disposeBag = DisposeBag()
    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        miniBarView.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.playerStartPlaying(notification:)), name: NSNotification.Name(rawValue: "MiniPlayerNotify"), object: nil)
        
        initComponents()
    }
    
    private func initComponents() {
        playBtn.rx.tap.bind {
            self.viewModel.doPlayButtonTap()
            self.setPlayButtonImage(isPlay: MusicHelper.instance.isPlaying())
        }.disposed(by: disposeBag)
        nextBtn.rx.tap.bind {
            self.viewModel.next()
            self.setSongInfo()
        }.disposed(by: disposeBag)
        previousBtn.rx.tap.bind {
            self.viewModel.previous()
            self.setSongInfo()
        }.disposed(by: disposeBag)
        MusicHelper.instance.player?.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions.new, context: nil)
        
        // or for swift 2 +
        let gesture = UITapGestureRecognizer(target: self, action:  #selector (self.minibarTapped (_:)))
        miniBarView.addGestureRecognizer(gesture)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "rate" {
            setSongInfo()
        }
    }
    
    @objc private func minibarTapped(_ sender:UITapGestureRecognizer){
        if let vc = self.storyBoard.instantiateViewController(withIdentifier: "MusicPlayerViewController") as? MusicPlayerViewController {
            vc.newSession = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    @objc private func playerStartPlaying(notification: Notification) {
        print(notification.userInfo ?? "")
        if let dict = notification.userInfo as NSDictionary? {
            if let isHidden = dict["isHidden"] as? Bool{
                miniBarView.isHidden = isHidden
                if !isHidden {
                    setSongInfo()
                }
            }
        }
    }
    
    private func setSongInfo() {
        let currentIndex = MusicHelper.instance.playingIndex.value
        if currentIndex > MusicHelper.instance.getListCount() - 1 {
            return
        }
        songNameLabel.text = MusicHelper.instance.musicList[currentIndex].track?.name ?? "unknown"
        setPlayButtonImage(isPlay: MusicHelper.instance.isPlaying())
    }
    
    private func setPlayButtonImage(isPlay: Bool) {
        playBtn.setImage(isPlay ? UIImage(named: "icon_pause.png") : UIImage(named: "icon_play.png"), for: .normal)
    }
}
