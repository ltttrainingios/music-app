//
//  LibraryViewController.swift
//  training_music_app
//
//  Created by Nguyễn  Mạnh Đức on 10/9/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift
import RxCocoa

class LibraryViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var playAllBtn: UIButton!
    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
    let viewModel = LibraryViewModel()
    
    var player:AVPlayer?
    var playButton:UIButton?
    var nextButton: UIButton?
    lazy var playerQueue : AVQueuePlayer = {
        return AVQueuePlayer()
    }()
    let disposeBag = DisposeBag()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: self.view.frame.size.height - 60)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(true, animated: true)
        tableView.register(UINib(nibName: "MusicTableViewCell", bundle: nil), forCellReuseIdentifier: "MusicTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self
        
        viewModel.musicList.asObservable().subscribe { (data) in
            self.tableView.reloadData()
        }
        .disposed(by: disposeBag)
        
        playAllBtn.rx.tap.bind {
            if let vc = self.storyBoard.instantiateViewController(withIdentifier: "MusicPlayerViewController") as? MusicPlayerViewController {
                vc.viewModel.musicList = self.viewModel.musicList.value
                vc.newSession = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        .disposed(by: disposeBag)
        
        viewModel.getMusicList()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension LibraryViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.musicList.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = "MusicTableViewCell"
        let cell: MusicTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? MusicTableViewCell
        cell.setData(item: viewModel.musicList.value[indexPath.row])
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.backgroundColor = UIColor.clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = self.storyBoard.instantiateViewController(withIdentifier: "MusicPlayerViewController") as? MusicPlayerViewController {
            var tempList = [Item]()
            tempList.append(viewModel.musicList.value[indexPath.row])
            vc.viewModel.musicList = tempList
            vc.newSession = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

