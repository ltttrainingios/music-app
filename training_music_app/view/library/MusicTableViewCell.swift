//
//  MusicTableViewCell.swift
//  training_music_app
//
//  Created by Nguyễn  Mạnh Đức on 10/10/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit
import Kingfisher

class MusicTableViewCell: UITableViewCell {
    @IBOutlet weak var musicImageView: UIImageView!
    @IBOutlet weak var musicNameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(item: Item) {
        musicNameLabel.text = item.track?.name ?? "unknown name"
        if let artist = item.track?.artists {
            artistLabel.text = artist[0].name ?? "unknown artist"
        }
        if let images = item.track?.album?.images {
            musicImageView.kf.setImage(with: URL(string: images[0].url ?? ""))
        }
        
    }
}
