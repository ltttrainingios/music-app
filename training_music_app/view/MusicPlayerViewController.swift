//
//  MusicPlayerViewController.swift
//  training_music_app
//
//  Created by Nguyễn  Mạnh Đức on 10/11/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit
import AVFoundation
import RxSwift
import RxCocoa
import Kingfisher

class MusicPlayerViewController: UIViewController {
    @IBOutlet weak var musicNameLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    @IBOutlet weak var musicImageView: UIImageView!
    @IBOutlet weak var previousBtn: UIButton!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var repeatBtn: UIButton!
    @IBOutlet weak var progressSlider: UISlider!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var shuffleBtn: UIButton!
    
    var newSession = false
    let disposeBag = DisposeBag()
    var viewModel = MusicPlayerViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideMiniPlayer(isHidden: true)
        if newSession {
            viewModel.addMusicListToQueue()
            viewModel.play()
            setPlayButtonImage(isPlay: true)
            setSongInfo(index: 0)
        } else {
            viewModel.musicList = MusicHelper.instance.musicList
            setPlayButtonImage(isPlay: MusicHelper.instance.isPlaying())
            setSongInfo(index: MusicHelper.instance.playingIndex.value)
        }
        initComponents()
        initSlider()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        hideMiniPlayer(isHidden: false)
    }
    
    private func hideMiniPlayer(isHidden: Bool) {
        let dataDict:[String: Bool] = ["isHidden": isHidden]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MiniPlayerNotify"), object: nil, userInfo: dataDict)
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        setPlayButtonImage(isPlay: viewModel.next())
    }
    
    func initComponents() {
        backBtn.rx.tap.bind {
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: true, completion: nil)
        }.disposed(by: disposeBag)
        playBtn.rx.tap.bind {
            self.playButtonTapped()
            }.disposed(by: disposeBag)
        nextBtn.rx.tap.bind {
            self.viewModel.next()
            }.disposed(by: disposeBag)
        previousBtn.rx.tap.bind {
            self.viewModel.previous()
            }.disposed(by: disposeBag)
        repeatBtn.rx.tap.bind {
            self.viewModel.changeRepeatType()
            }.disposed(by: disposeBag)
        shuffleBtn.rx.tap.bind {
            self.viewModel.changeShuffle()
        }.disposed(by: disposeBag)
        MusicHelper.instance.playingIndex.asObservable().subscribe(onNext: { (data) in
            self.setSongInfo(index: data)
        }).disposed(by: disposeBag)
        MusicHelper.instance.repeatType.asObservable().subscribe(onNext: { (data) in
            self.changeRepeatIcon(repeatType: data)
        }).disposed(by: disposeBag)
        MusicHelper.instance.isShuffle.asObservable().subscribe(onNext: { (data) in
            self.changeShuffleIcon(isShuffle: data)
        }).disposed(by: disposeBag)
        MusicHelper.instance.player?.addObserver(self, forKeyPath: "rate", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    func changeRepeatIcon(repeatType: RepeatType) {
        switch repeatType {
        case .NO_REPEAT:
            repeatBtn.setBackgroundImage(UIImage(named: "icon_no_repeat.png"), for: .normal)
        case .REPEAT_ONE:
            repeatBtn.setBackgroundImage(UIImage(named: "icon_repeat_one.png"), for: .normal)
        case .REPEAT_ALL:
            repeatBtn.setBackgroundImage(UIImage(named: "icon_repeat_all.png"), for: .normal)
        }
    }

    func changeShuffleIcon(isShuffle: Bool) {
        shuffleBtn.setBackgroundImage(isShuffle ?
                UIImage(named: "icon_shuffle.png") : UIImage(named: "icon_no_shuffle.png"), for: .normal)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "rate" {
            setPlayButtonImage(isPlay: MusicHelper.instance.player!.rate > 0.0 ? true : false)
        }
    }
    
    func initSlider() {
        progressSlider.minimumValue = 0
        let duration : CMTime = MusicHelper.instance.player?.currentItem?.asset.duration ?? CMTime.init()
        let seconds : Float64 = CMTimeGetSeconds(duration)
        progressSlider.maximumValue = Float(seconds)
        progressSlider.isContinuous = false
        progressSlider.tintColor = UIColor.green
        progressSlider.addTarget(self, action: #selector(playbackSliderValueChanged(_:)), for: .valueChanged)
        MusicHelper.instance.player?.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            if MusicHelper.instance.player?.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds((MusicHelper.instance.player?.currentTime())!)
                self.progressSlider.value = Float ( time )
            }
        }
    }
    
    @objc func playbackSliderValueChanged(_ playbackSlider:UISlider)
    {
        let seconds : Int64 = Int64(playbackSlider.value)
        let targetTime:CMTime = CMTimeMake(seconds, 1)
        MusicHelper.instance.player?.seek(to: targetTime)
        if MusicHelper.instance.player?.rate == 0
        {
            MusicHelper.instance.player?.play()
        }
    }
    
    func playButtonTapped()
    {
        MusicHelper.instance.player?.rate == 0 ? MusicHelper.instance.player?.play() : MusicHelper.instance.player?.pause()
    }
    
    func setPlayButtonImage(isPlay: Bool) {
        playBtn.setBackgroundImage(isPlay ? UIImage(named: "icon_pause.png") : UIImage(named: "icon_play.png"), for: .normal)
    }
    
    func setSongInfo(index: Int) {
        if index > viewModel.musicList.count - 1 {
            return
        }
        musicNameLabel.text = viewModel.musicList[index].track?.name ?? "unknown name"
        if let artist = viewModel.musicList[index].track?.artists {
            artistNameLabel.text = artist[0].name ?? "unknown artist"
        }
        if let images = viewModel.musicList[index].track?.album?.images {
            musicImageView.kf.setImage(with: URL(string: images[0].url ?? ""))
        }
    }
    
    deinit {
        MusicHelper.instance.player?.removeObserver(self, forKeyPath: "rate")
    }
}
