//
//  MusicModel.swift
//  training_music_app
//
//  Created by Nguyễn  Mạnh Đức on 10/10/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation

class MusicModel: Codable {
    let href: String?
    let items: [Item]?
    let limit: Int?
    let offset: Int?
    let total: Int?
    
    init(href: String?, items: [Item]?, limit: Int?, offset: Int?, total: Int?) {
        self.href = href
        self.items = items
        self.limit = limit
        self.offset = offset
        self.total = total
    }
}

class Item: Codable {
    let addedAt: String?
    let track: Track?
    
    enum CodingKeys: String, CodingKey {
        case addedAt = "added_at"
        case track
    }
    
    init(addedAt: String?, track: Track?) {
        self.addedAt = addedAt
        self.track = track
    }
}

class Track: Codable {
    let album: Album?
    let artists: [Artist]?
    let availableMarkets: [String]?
    let discNumber, durationMS: Int?
    let explicit: Bool?
    let externalIDS: ExternalIDS?
    let externalUrls: ExternalUrls?
    let href: String?
    let id: String?
    let isLocal: Bool?
    let name: String?
    let popularity: Int?
    let previewURL: String?
    let trackNumber: Int?
    let type, uri: String?
    
    enum CodingKeys: String, CodingKey {
        case album, artists
        case availableMarkets = "available_markets"
        case discNumber = "disc_number"
        case durationMS = "duration_ms"
        case explicit
        case externalIDS = "external_ids"
        case externalUrls = "external_urls"
        case href, id
        case isLocal = "is_local"
        case name, popularity
        case previewURL = "preview_url"
        case trackNumber = "track_number"
        case type, uri
    }
    
    init(album: Album?, artists: [Artist]?, availableMarkets: [String]?, discNumber: Int?, durationMS: Int?, explicit: Bool?, externalIDS: ExternalIDS?, externalUrls: ExternalUrls?, href: String?, id: String?, isLocal: Bool?, name: String?, popularity: Int?, previewURL: String?, trackNumber: Int?, type: String?, uri: String?) {
        self.album = album
        self.artists = artists
        self.availableMarkets = availableMarkets
        self.discNumber = discNumber
        self.durationMS = durationMS
        self.explicit = explicit
        self.externalIDS = externalIDS
        self.externalUrls = externalUrls
        self.href = href
        self.id = id
        self.isLocal = isLocal
        self.name = name
        self.popularity = popularity
        self.previewURL = previewURL
        self.trackNumber = trackNumber
        self.type = type
        self.uri = uri
    }
}

class Album: Codable {
    let albumType: String?
    let artists: [Artist]?
    let availableMarkets: [String]?
    let externalUrls: ExternalUrls?
    let href: String?
    let id: String?
    let images: [Image]?
    let name, releaseDate, releaseDatePrecision: String?
    let totalTracks: Int?
    let type, uri: String?
    
    enum CodingKeys: String, CodingKey {
        case albumType = "album_type"
        case artists
        case availableMarkets = "available_markets"
        case externalUrls = "external_urls"
        case href, id, images, name
        case releaseDate = "release_date"
        case releaseDatePrecision = "release_date_precision"
        case totalTracks = "total_tracks"
        case type, uri
    }
    
    init(albumType: String?, artists: [Artist]?, availableMarkets: [String]?, externalUrls: ExternalUrls?, href: String?, id: String?, images: [Image]?, name: String?, releaseDate: String?, releaseDatePrecision: String?, totalTracks: Int?, type: String?, uri: String?) {
        self.albumType = albumType
        self.artists = artists
        self.availableMarkets = availableMarkets
        self.externalUrls = externalUrls
        self.href = href
        self.id = id
        self.images = images
        self.name = name
        self.releaseDate = releaseDate
        self.releaseDatePrecision = releaseDatePrecision
        self.totalTracks = totalTracks
        self.type = type
        self.uri = uri
    }
}

class Artist: Codable {
    let externalUrls: ExternalUrls?
    let href: String?
    let id, name: String?
    let type: TypeEnum?
    let uri: String?
    
    enum CodingKeys: String, CodingKey {
        case externalUrls = "external_urls"
        case href, id, name, type, uri
    }
    
    init(externalUrls: ExternalUrls?, href: String?, id: String?, name: String?, type: TypeEnum?, uri: String?) {
        self.externalUrls = externalUrls
        self.href = href
        self.id = id
        self.name = name
        self.type = type
        self.uri = uri
    }
}

class ExternalUrls: Codable {
    let spotify: String?
    
    init(spotify: String?) {
        self.spotify = spotify
    }
}

enum TypeEnum: String, Codable {
    case artist = "artist"
}

class Image: Codable {
    let height: Int?
    let url: String?
    let width: Int?
    
    init(height: Int?, url: String?, width: Int?) {
        self.height = height
        self.url = url
        self.width = width
    }
}

class ExternalIDS: Codable {
    let isrc: String?
    
    init(isrc: String?) {
        self.isrc = isrc
    }
}

// MARK: Encode/decode helpers

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
