//
//  MusicPlayerViewModel.swift
//  training_music_app
//
//  Created by Nguyễn  Mạnh Đức on 10/11/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum RepeatType {
    case NO_REPEAT
    case REPEAT_ONE
    case REPEAT_ALL
}

class MusicPlayerViewModel {
    var musicList = [Item]()
    
    func addMusicListToQueue() {
        MusicHelper.instance.addMusicListToQueue(list: musicList)
    }
    
    func play() {
        MusicHelper.instance.play()
    }
    
    func pause() {
        MusicHelper.instance.player?.pause()
    }
    
    func next() -> Bool {
        return MusicHelper.instance.next()
    }
    
    func previous() {
        MusicHelper.instance.previous()
    }

    func changeRepeatType() {
        MusicHelper.instance.changeRepeatType()
    }

    func changeShuffle() {
        MusicHelper.instance.changeShuffle()
    }
}
