//
//  MainViewModel.swift
//  training_music_app
//
//  Created by Nguyễn  Mạnh Đức on 10/23/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class MainViewModel {
    private func play() {
        MusicHelper.instance.player?.play()
    }
    
    private func pause() {
        MusicHelper.instance.player?.pause()
    }
    
    func doPlayButtonTap() {
        MusicHelper.instance.isPlaying() ? pause() : play()
    }
    
    func next() {
        MusicHelper.instance.next()
    }
    
    func previous() {
        MusicHelper.instance.previous()
    }
}
