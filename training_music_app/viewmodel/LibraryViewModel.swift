//
//  LibraryViewModel.swift
//  training_music_app
//
//  Created by Nguyễn  Mạnh Đức on 10/11/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class LibraryViewModel {
    var musicList = BehaviorRelay<[Item]>(value: [Item]())
    
    func getMusicList() {
        ApiHelper.instance.getMusicList { (data) in
            self.musicList.accept(data)
        }
    }
}
