//
//  ApiHelper.swift
//  training_music_app
//
//  Created by Nguyễn  Mạnh Đức on 10/10/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation
import Alamofire

class ApiHelper {
    static let instance = ApiHelper()
    
    final let BASE_URL = "https://api.spotify.com/v1/"
    let headers = [
        "Authorization":"Bearer BQBz8q69pt0tNl5KfhAju_mrLeAWN3NkHU41JHeyD9OCgUIz_I66I6BnhMQ9f1Sw28jXEQLYOhWcduTulVgsA_tAuq7LKBx0Krl5lrkHUT4LzwbC7zq9Vtp2Ss8gh31oFXer2pCkE6H-DOgJmXKz4TlkH5UcnwC_svARQsQYmDefjw_AEcQrQkw"
    ]
    let parameters = [
        "limit":"30"
    ]
    
    func getMusicList(completionHandler: @escaping (_ point: [Item]) -> Void) {
        
        let url = "\(BASE_URL)me/tracks"
        Alamofire.request(url, method: .get, parameters: parameters, headers: headers).responseJSON { response in
            switch(response.result) {
            case .success(_):
                print(response.result.value ?? "")
                if let data = response.data {
                    let photoModel = try? JSONDecoder().decode(MusicModel.self, from: data)
                    if let photoModel = photoModel {
                        //handle data here
                        completionHandler(self.removeInvalidData(musicModel: photoModel))
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func removeInvalidData(musicModel: MusicModel?) -> [Item] {
        guard let item = musicModel?.items else { return [] }
        return item.filter{ $0.track?.previewURL != nil}
    }
}
